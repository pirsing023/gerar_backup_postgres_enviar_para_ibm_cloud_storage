#!/bin/bash

dataEHora=`date +%Y%m%d_%H%M` 
extensao='.dmp.zip'
diretorio=""
arquivo='nomeDoArquivo_'
nomeArquivo=$arquivo$dataEHora$extensao

regiaoBucket=''
ibmCloudApiKey=''

function login_ibmcloud {
    quantidade_de_tentativas=3
    while [ $quantidade_de_tentativas -gt 0 ]; do
        ibmcloud login --apikey $ibmCloudApiKey -r regiao_bucket > /dev/null 2>&1
        if [ $? -eq 0 ]; then
            return 0
        fi
        ((quantidade_de_tentativas--))
        sleep 5
    done
    return 1
}

PGPASSWORD="senhaDobanco" pg_dump -h host -p porta --format=c --blobs -U postgres -d nome_base_de_dados | gzip > "$diretorio/$nomeArquivo"

if [ -e "$diretorio/$nomeArquivo" ]; then
    login_ibmcloud
    if [ $? -eq 0 ]; then
        ibmcloud cos upload --bucket backup-postgres --key $nomeArquivo --file $diretorio/$nomeArquivo --region $regiaoBucket > /dev/null
        rm "$diretorio/$nomeArquivo"
    fi
fi

